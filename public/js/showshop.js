var cart = {}; //моя корзина

$('document').ready(function () {
    loadGoods();
    checkCart();
    showMiniCart();
});

function loadGoods() {
    //загружаю товары на страницу
    $.getJSON('data.json', function (data) {
        var out = '';
        for (var key in data) {


            out += '<div class="col-md-4">';
            out += '<div class="card mb-4 shadow-sm">';
            out += '<div class="slider__item">';
            out += '<img class="slider__img" src="' + data[key].image + '">';
            out += '<div class="slider__desc">';
            out += '<h3>' + data[key]['name'] + '</h3>';
            out += '<p>Цена $ <span class="slider__price">' + data[key]['price'] + '</span></p>';
            out += '<span>Артикул: ' + data[key]['art'] + '</span>';
            out += '</div>';
            out += '</div>';
            out += '<div class="card-body">';
            out += '<p class="card-text">' + data[key]['description'] + '</p>';
            out += '<div class="form-group">'
            out += '<label class="card-input">Введите количество товара'
            out += '<input class="form-control" class="input"/>'
            out += '</label>'
            out += '</div>'
            out += '<div class="d-flex justify-content-between align-items-center">'
            out += '<div class="btn-group slider__buttons">'
            out += '<button type="button" class="btn btn-sm btn-outline-secondary  add-to-cart" data-art="' + key + '">Добавить в корзину</button>'
            out += '</div>'
            out += '</div>'
            out += '</div>'
            out += '</div>'
            out += '</div>'
        }
        $('#goods').html(out);
        $('button.add-to-cart').on('click', addToCart);
        $('.slider__img').on('click', showPicture);
    });
}

function addToCart() {
    //добавляем товар в корзину
    var articul = $(this).attr('data-art');
    if (cart[articul] != undefined) {
        cart[articul]++;
    } else {
        cart[articul] = 1;
    }
    localStorage.setItem('cart', JSON.stringify(cart));
   
    showMiniCart();
    window.location.reload();
    
}

function checkCart() {
    //проверяю наличие корзины в localStorage;
    if (localStorage.getItem('cart') != null) {
        cart = JSON.parse(localStorage.getItem('cart'));
    }
}

function showMiniCart() {
    //показываю содержимое корзины
    var out = '';
    for (var w in cart) {
        out += w + ' --- ' + cart[w] + '<br>';
    }
    $('#mini-cart').html(out);
}

function showPicture() {
    $(".slider__img").click(function () {

        let img = $(this);
        let src = img.attr('src');
        $("body").append("<div class='popup'>" +
            "<div class='popup_bg'></div>" +
            "<img src='" + src + "' class='popup_img' />" +
            "</div>");
        $(".popup").fadeIn(200);
        $(".popup_bg").click(function () {
            $(".popup").fadeOut(200);
            setTimeout(function () {
                $(".popup").remove();
            }, 200);
        });
    });
}