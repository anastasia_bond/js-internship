const phoneMaskSelector = '.js-phone-input';
const phoneMaskInputs = document.querySelectorAll(phoneMaskSelector);

const masksOptions = {
  phone: {
    mask: '+{7} (000) 000-00-00'
  }
};

for (const item of phoneMaskInputs) {
  new IMask(item, masksOptions.phone);
}